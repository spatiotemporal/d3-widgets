# D3 Widgets

This widget library is written using the D3 framework, and is targeted for web sites using D3 as their main framework.

## Licensing

D3 Widgets is covered by the [LGPL v3](LICENSE) license, or any later version of that license.
