define(['require', 'd3'], function(require) {

    var d3 = require('d3');

    var instanceID = 0;

    function radioButtonGroup(buttons, preselected) {
        var self = this;
        self.groupId = "d3-radiobuttongroup-" + instanceID++;
        self.buttons = buttons;
        self.onChange = null;
        self.selected = null;

        if (preselected === undefined && self.buttons.length > 0) {
            preselected = self.buttons[0].id;
        }

        self.root = d3.create("div")
            .attr("class", "d3-widget-radiobuttongroup")
            .style("position", "relative")
            .style("display", "inline-block")
            .style("box-sizing", "border-box");

        for (let btn of self.buttons) {
            var buttonId = self.groupId + "-" + btn.id;
            self.root.append("input")
                .attr("type", "radio")
                .attr("name", self.groupId)
                .attr("value", btn.id)
                .attr("id", buttonId)
                .property("checked", btn.id == preselected)
                .on("change", function(e) {
                    var id = d3.select(d3.event.target).property("value");
                    self.selected = id;
                    if (self.onChange !== null) {
                        self.onChange(id);
                    }
                });
            if (btn.id == preselected) {
                self.selected = preselected;
            }
            self.root.append("label")
                .attr("for", buttonId)
                .html(btn.label);
        }
    }

    radioButtonGroup.prototype.node = function() { var root = this.root; return function() { return root.node(); }; };

    radioButtonGroup.prototype.get = function() {
        return this.selected;
    }

    radioButtonGroup.prototype.set = function(id, invokeCallback = true) {
        var btn = this.root.select("#" + this.groupId + "-" + id);
        if (btn.node() === null) {
            console.error("Invalid ID '" + id + "'");
            return;
        }
        this.selected = id;
        btn.property("checked", true);
        if (invokeCallback) {
            btn.dispatch("change");
        }
    }

    radioButtonGroup.prototype.on = function(event, callback) {
        switch (event) {
        case "change": this.onChange = callback; break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    radioButtonGroup.prototype.dispatch = function(event) {
        switch (event) {
        case "change": if (this.onChange !== null) this.onChange(this.selected); break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    return radioButtonGroup;
});
