define(['require', 'd3'], function(require) {

    var d3 = require('d3');

    const mod = (x, n) => (x % n + n) % n;

    function iconSelect(sets) {
        var self = this;
        self.isOpen = false;
        self.focusTimeout = null;
        self.onChange = null;
        self.selection = [];
        self.previewUpdater = null;
        self.rightAlign = false;
        if (sets !== undefined) {
            self.sets = sets;
        } else {
            self.sets = [];
        }

        self.root = d3.create("div")
            .attr("class", "d3-widget-iconselect")
            .attr("tabindex", "0")
            .style("position", "relative")
            .style("display", "inline-block")
            .style("white-space", "nowrap")
            .style("box-sizing", "border-box")
            .style("user-select", "none")
            .on("focus", function() {
                if (self.focusTimeout !== null) {
                    clearTimeout(self.focusTimeout);
                    self.focusTimeout = null;
                }
                self.open();
            })
            .on("focusout", function() {
                if (self.focusTimeout === null) {
                    self.focusTimeout = setTimeout(function() {
                        self.focusTimeout = null;
                        self.close();
                    }, 0, true);
                }
            })
            .on("keydown", function() {
                switch (d3.event.keyCode) {
                case 27: // Escape
                    self.root.node().focus();
                    self.close();
                    break;
                case 13: // Enter
                case 32: // Space
                    if (d3.event.target.tagName.toLowerCase() != "button") {
                        if (self.isOpen) {
                            self.close();
                        } else {
                            self.open();
                        }
                    }
                    break;
                case 37: // Left arrow
                    self.moveFocus(d3.event.target, -1, 0);
                    break;
                case 39: // Right arrow
                    self.moveFocus(d3.event.target, +1, 0);
                    break;
                case 38: // Up arrow
                    self.moveFocus(d3.event.target, 0, -1);
                    break;
                case 40: // Down arrow
                    self.moveFocus(d3.event.target, 0, +1);
                    break;
                }
            });

        self.current = self.root.append("button")
            .attr("class", "d3-widget-iconselect-preview")
            .style("display", "inline-block")
            .style("box-sizing", "border-box")
            .on("mousedown", function() {
                if (self.isOpen) {
                    self.close();
                } else {
                    self.open();
                }
            });

        self.iconArea = self.root.append("div")
            .attr("class", "d3-widget-iconarea")
            .style("position", "absolute")
            .style("box-sizing", "border-box")
            .style("z-index", "1000")
            .style("top", "100%")
            .style("left", "0")
            .style("right", "auto")
            .style("max-height", "0")
            .style("visibility", "hidden");

        self.updateCurrent = function() {
            self.current.html(null);
            if (self.previewUpdater !== null) {
                self.previewUpdater(self.current, self.selection);
            } else {
                self.current.selectAll("span").data(self.sets).enter()
                    .append("span")
                    .each(function(d, i) {
                        d.entries.find(e => (e.id == self.selection[i])).construct(d3.select(this));
                    });
            }
        };

        self.updateSelection = function() {
            self.iconArea.selectAll(function() {
                return this.childNodes;
            }).each(function(d, i) {
                d3.select(this).selectAll("button")
                    .each(function(d, i) {
                        d3.select(this)
                            .attr("class", d.id == self.selection[d.set] ? "d3-widget-button-selected" : null);
                    });
            });
            self.updateCurrent();
        };

        self.updateIcons = function() {
            self.iconArea.selectAll("div").remove();
            self.iconArea.selectAll("div").data(self.sets).enter()
                .append("div")
                .append("span")
                .style("float", d => (self.rightAlign ? "right" : "left"))
                .selectAll("button")
                .data((d, i, j) => d.entries.map(e => Object.assign(e, { set: i })))
                .enter()
                .append("button")
                .attr("data-set", d => d.set)
                .attr("data-entry", (d, i) => i)
                .each(function(d, i) {
                    d.construct(d3.select(this));
                })
                .on("focus", function(e) {
                    if (self.focusTimeout !== null) {
                        clearTimeout(self.focusTimeout);
                        self.focusTimeout = null;
                    }
                })
                .on("click", function(d) {
                    self.selection[d.set] = d.id;
                    self.updateSelection();
                    if (self.onChange !== null) {
                        self.onChange(self.selection);
                    }
                });
            self.selection = self.sets.map(s => s.entries[0].id);
            self.updateSelection();
        };

        self.moveFocus = function(element, x, y) {
            var set = (y < 0) ? 0 : self.sets.length - 1;
            var entry = self.rightAlign ? (self.sets[set].entries.length - 1) : 0;
            if (d3.event.target.tagName.toLowerCase() == "button") {
                set = parseInt(d3.select(d3.event.target).attr("data-set"));
                entry = parseInt(d3.select(d3.event.target).attr("data-entry"));
            } else if (y == 0) {
                return;
            }

            if (self.rightAlign) {
                entry = self.sets[set].entries.length - 1 - entry;
            }
            set = mod((set + y),  self.sets.length);
            if (entry > self.sets[set].entries.length - 1) {
                entry = self.sets[set].entries.length - 1;
            }
            if (self.rightAlign) {
                entry = self.sets[set].entries.length - 1 - entry;
            }
            entry = mod((entry + x), self.sets[set].entries.length);
            self.iconArea.node().childNodes[set].childNodes[0].childNodes[entry].focus();
        }

        self.open = function() {
            self.iconArea
                .style("max-height", "30em")
                .style("visibility", "");
            self.isOpen = true;
        };

        self.close = function() {
            self.iconArea
                .style("max-height", "0")
                .style("visibility", "hidden");
            self.isOpen = false;
        };

        self.updateIcons();

        new ResizeObserver(function(entries) {
            for (let e of entries) {
                var relRect = e.contentRect;
                var absRect = e.target.parentElement.getBoundingClientRect();
                var elem = d3.select(e.target);
                var vpWidth = window.innerWidth;

                if (self.rightAlign) {
                    if (absRect.x - relRect.width < 0) {
                        elem.style("right", (absRect.x - relRect.width) + "px");
                    } else {
                        elem.style("right", "0");
                    }
                } else {
                    if (absRect.x + relRect.width > vpWidth) {
                        elem.style("left", (vpWidth - absRect.x - relRect.width) + "px");
                    } else {
                        elem.style("left", "0");
                    }
                }
            }
        }).observe(self.iconArea.node());

    }

    iconSelect.prototype.node = function() { var root = this.root; return function() { return root.node(); }; };

    iconSelect.prototype.setIcons = function(sets) {
        this.sets = sets;
        this.updateIcons()
    };

    iconSelect.prototype.setPreviewUpdater = function(func) {
        this.previewUpdater = func;
        this.updateCurrent();
    };

    iconSelect.prototype.getSelection = function() {
        return this.selection;
    };

    iconSelect.prototype.setSelection = function(selection) {
        this.selection = selection;
        this.updateSelection();
    };

    iconSelect.prototype.setAlign = function(align) {
        switch (align) {
        case "left": this.rightAlign = false; break;
        case "right": this.rightAlign = true; break;
        default: console.error("Unknown alignment '" + align + "'"); return;
        }

        if (this.rightAlign) {
            this.iconArea.style("left", "auto")
            this.iconArea.style("right", "0")
        } else {
            this.iconArea.style("left", "0")
            this.iconArea.style("right", "auto")
        }
        this.updateIcons();
    }

    iconSelect.prototype.on = function(event, callback) {
        switch (event) {
        case "change": this.onChange = callback; break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    iconSelect.prototype.dispatch = function(event) {
        switch (event) {
        case "change": if (this.onChange !== null) this.onChange(this.selection); break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    return iconSelect;
});
