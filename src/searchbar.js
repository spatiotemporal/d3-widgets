define(['require', 'd3'], function(require) {

    var d3 = require('d3');

    const DEFAULT_SEARCH_DELAY = 500;
    const DEFAULT_MIN_INPUT_CHARACHTERS = 3;

    function searchBar(entries) {
        var self = this;
        self.isOpen = false;
        self.focusTimeout = null;
        self.inputTimeout = null;
        self.searchDelay = DEFAULT_SEARCH_DELAY;
        self.minInputCharacters = DEFAULT_MIN_INPUT_CHARACHTERS;
        self.onSubmit = null;
        self.onSearch = null;
        self.queryNumber = 0;
        self.receivedQuery = -1;
        self.submitByEntryMode = false;
        self.frozenValueMode = false;
        self.frozen = false;

        if (entries !== undefined) {
            self.entries = entries;
        } else {
            self.entries = null;
        }

        self.root = d3.create("div")
            .attr("class", "d3-widget-searchbar")
            .style("position", "relative")
            .style("display", "inline-block")
            .style("box-sizing", "border-box")
            .style("white-space", "nowrap")
            .on("keydown", function() {
                switch (d3.event.keyCode) {
                case 27: // Escape
                    if (self.frozen) {
                        self.closeButton.dispatch("click");
                    }
                    self.textField.node().focus();
                    break;
                case 13: // Enter
                    if (self.frozen) {
                        break;
                    }
                    if (d3.event.target.tagName.toLowerCase() == "input") {
                        if (!self.submitByEntryMode) {
                            self.submit(self.textField.property("value"), self.textField.property("value"));
                        }
                    } else {
                        d3.select(d3.event.target).dispatch("click");
                    }
                    break;
                case 38: // Up arrow
                    if (d3.event.target.tagName.toLowerCase() == "li" && d3.event.target.previousSibling === null) {
                        self.textField.node().focus();
                    } else if (d3.event.target.tagName.toLowerCase() == "li") {
                        d3.event.target.previousSibling.focus();
                    } else if (self.list.node().childElementCount > 0) {
                        self.list.node().lastChild.focus();
                    }
                    break;
                case 40: // Down arrow
                    if (d3.event.target.tagName.toLowerCase() == "li" && d3.event.target.nextSibling === null) {
                        self.textField.node().focus();
                    } else if (d3.event.target.tagName.toLowerCase() == "li") {
                        d3.event.target.nextSibling.focus();
                    } else if (self.list.node().childElementCount > 0) {
                        self.list.node().firstChild.focus();
                    }
                    break;
                }
            });

        self.textField = self.root.append("input")
            .attr("type", "text")
            .style("box-sizing", "border-box")
            .on("focus", function() {
                if (self.focusTimeout !== null) {
                    clearTimeout(self.focusTimeout);
                    self.focusTimeout = null;
                }
                self.open();
            })
            .on("focusout", function() {
                if (self.focusTimeout === null) {
                    self.focusTimeout = setTimeout(function() {
                        self.focusTimeout = null;
                        self.close();
                    }, 0, true);
                }
            })
            .on("input", function() {
                var searchString = d3.event.target.value;
                if (searchString.length >= self.minInputCharacters) {
                    if (self.onSearch !== null) {
                        if (self.inputTimeout !== null) {
                            clearTimeout(self.inputTimeout);
                        }
                        self.inputTimeout = setTimeout(function() {
                            var qn = self.queryNumber++;
                            self.onSearch(searchString).then(function(matches) {
                                if (qn > self.receivedQuery) {
                                    self.receivedQuery = qn;
                                    self.updateMatches(matches);
                                }
                            });
                            self.inputTimeout = null;
                        }, self.searchDelay);
                    } else if (self.entries !== null) {
                        self.updateMatches(self.entries.filter(function(e) {
                            return searchString.toLowerCase().split(" ")
                                .every(term => (e.name.toLowerCase().indexOf(term) >= 0));
                        }));
                    }
                } else {
                    self.receivedQuery = self.queryNumber++;
                    self.updateMatches([]);
                }
            });

        self.list = self.root.append("ul")
            .style("position", "absolute")
            .style("box-sizing", "border-box")
            .style("z-index", "1000")
            .style("top", "100%")
            .style("left", "0")
            .style("max-height", "0")
            .style("visibility", "hidden")
            .style("list-style", "none")
            .style("margin", "0")
            .style("padding", "0")
            .style("min-width", "100%")
            .style("overflow-x", "hidden")
            .style("overflow-y", "auto");

        self.valueBox = self.root.append("span")
            .attr("class", "d3-widget-searchbar-frozen-value")
            .style("display", "none")
            .style("grid-template-columns", "auto min-content")
            .style("position", "absolute")
            .style("top", "50%")
            .style("left", "0")
            .style("right", "0")
            .style("transform", "translateY(-50%)")
            .style("padding-left", "0.5em")
            .style("padding-right", "0.5em")
            .style("padding-top", "0.1em")
            .style("padding-bottom", "0.1em")
            .style("user-select", "none");

        self.valueBoxText = self.valueBox.append("span")
            .style("text-overflow", "ellipsis")
            .style("overflow", "hidden");

        self.closeButton = self.valueBox.append("span")
            .attr("class", "d3-widget-close-button")
            .style("margin-left", "0.6em")
            .html("&#xD7;")
            .on("click", function() {
                self.frozen = false;
                self.textField.property("readOnly", false);
                self.valueBox.style("display", "none");
                self.textField.node().focus();
                if (self.onSubmit !== null) {
                    self.onSubmit(null);
                }
            });

        self.submit = function(value, text) {
            self.textField.property("value", "");
            self.textField.dispatch("input");
            self.textField.node().focus();
            if (self.frozenValueMode) {
                self.frozen = true;
                self.textField.property("readOnly", true);
                self.valueBox.style("display", "grid");
                self.valueBoxText.html(text);
            }
            if (self.onSubmit !== null) {
                self.onSubmit(value);
            }
        }

        self.updateMatches = function(matches) {
            self.list.selectAll("li").remove();
            var li = self.list.selectAll("li").data(matches).enter().append("li")
                .attr("tabindex", "0")
                .style("user-select", "none")
                .html(m => m.name)
                .on("mouseenter", function() {
                    d3.event.target.focus();
                })
                .on("focus", function(e) {
                    if (self.focusTimeout !== null) {
                        clearTimeout(self.focusTimeout);
                        self.focusTimeout = null;
                    }
                    d3.select(d3.event.target).attr("class", "d3-widget-entry-selected");
                })
                .on("focusout", function(e) {
                    if (self.focusTimeout === null) {
                        self.focusTimeout = setTimeout(function() {
                            self.focusTimeout = null;
                            self.close();
                        }, 0, true);
                    }
                    d3.select(d3.event.target).attr("class", null);
                })
                .on("click", function(e) {
                    if (self.submitByEntryMode) {
                        self.submit(e, e.name);
                    } else {
                        self.submit(e.name, e.name);
                    }
                });
            self.open();
        }

        self.open = function() {
            self.list
                .style("max-height", "30em")
                .style("visibility", "");
            self.isOpen = true;
        };

        self.close = function() {
            self.list
                .style("max-height", "0")
                .style("visibility", "hidden");
            self.isOpen = false;
        };

        new ResizeObserver(function(entries) {
            for (let e of entries) {
                var relRect = e.contentRect;
                var absRect = e.target.parentElement.getBoundingClientRect();
                var elem = d3.select(e.target);
                var vpWidth = window.innerWidth;

                if (absRect.x + relRect.width > vpWidth) {
                    elem.style("left", (vpWidth - absRect.x - relRect.width) + "px");
                } else {
                    elem.style("left", "0");
                }
            }
        }).observe(self.list.node());
    }

    searchBar.prototype.node = function() { var root = this.root; return function() { return root.node(); }; };

    searchBar.prototype.setEntries = function(entries) {
        this.entries = entries;
    }

    searchBar.prototype.setMinInputCharacters = function(characters) {
        if (characters === undefined) {
            this.minInputCharacters = DEFAULT_MIN_INPUT_CHARACHTERS;
        } else {
            this.minInputCharacters = characters;
        }
    }

    searchBar.prototype.setSearchDelay = function(delay_ms) {
        if (delay_ms === undefined) {
            this.searchDelay = DEFAULT_SEARCH_DELAY;
        } else {
            this.searchDelay = delay_ms;
        }
    }

    searchBar.prototype.setSubmitByEntryMode = function(modeEnabled) {
        this.submitByEntryMode = !!modeEnabled;
    };

    searchBar.prototype.setFrozenValueMode = function(modeEnabled) {
        this.frozenValueMode = !!modeEnabled;
    };

    searchBar.prototype.clear = function() {
        this.frozen = false;
        this.textField.property("readOnly", false);
        this.valueBox.style("display", "none");
        this.textField.property("value", "");
    };

    searchBar.prototype.on = function(event, callback) {
        switch (event) {
        case "submit": this.onSubmit = callback; break;
        case "search": this.onSearch = callback; break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    searchBar.prototype.dispatch = function(event) {
        switch (event) {
        case "submit":
            if (!this.submitByEntryMode) {
                this.submit(this.textField.property("value"), this.textField.property("value"));
            }
            break;
        case "search":
            this.textField.dispatch("input");
            break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    return searchBar;
});
