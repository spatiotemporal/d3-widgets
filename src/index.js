define(['require', './multiselect', './searchbar', './iconselect', './radiobuttongroup'], function(require) {

    return {
        multiSelect: require('./multiselect'),
        searchBar: require('./searchbar'),
        iconSelect: require('./iconselect'),
        radioButtonGroup: require('./radiobuttongroup'),
    };
});
