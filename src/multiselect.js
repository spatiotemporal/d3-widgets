define(['require', 'd3'], function(require) {

    var d3 = require('d3');

    const DEFAULT_NONE_LABEL = "None";
    const DEFAULT_ALL_LABEL = "All";

    var instanceID = 0;

    function multiSelect(entries) {
        var self = this;
        self.id = instanceID++;
        self.isOpen = false;
        self.focusTimeout = null;
        self.onChange = null;
        self.noneLabel = DEFAULT_NONE_LABEL;
        self.allEnabled = false;
        if (entries !== undefined) {
            self.entries = entries;
        } else {
            self.entries = [];
        }

        self.root = d3.create("div")
            .attr("class", "d3-widget-multiselect")
            .attr("tabindex", "0")
            .style("position", "relative")
            .style("display", "inline-block")
            .style("box-sizing", "border-box")
            .style("white-space", "nowrap")
            .on("focus", function() {
                if (self.focusTimeout !== null) {
                    clearTimeout(self.focusTimeout);
                    self.focusTimeout = null;
                }
                self.open();
            })
            .on("focusout", function() {
                if (self.focusTimeout === null) {
                    self.focusTimeout = setTimeout(function() {
                        self.focusTimeout = null;
                        self.close();
                    }, 0, true);
                }
            })
            .on("keydown", function() {
                switch (d3.event.keyCode) {
                case 27: // Escape
                    self.root.node().focus();
                    self.close();
                    break;
                case 13: // Enter
                    if (d3.event.target.tagName.toLowerCase() == "input") {
                        d3.event.target.checked = !d3.event.target.checked;
                        d3.select(d3.event.target).dispatch("change");
                    } else {
                        if (self.isOpen) {
                            self.close();
                        } else {
                            self.open();
                        }
                    }
                    break;
                case 32: // Space
                    if (d3.event.target.tagName.toLowerCase() != "input") {
                        if (self.isOpen) {
                            self.close();
                        } else {
                            self.open();
                        }
                    }
                    break;
                case 38: // Up arrow
                    if (d3.event.target.tagName.toLowerCase() == "input" && d3.event.target.parentElement.previousSibling !== null) {
                        d3.event.target.parentElement.previousSibling.firstChild.focus();
                    } else {
                        self.list.node().lastChild.firstChild.focus();
                    }
                    break;
                case 40: // Down arrow
                    if (d3.event.target.tagName.toLowerCase() == "input" && d3.event.target.parentElement.nextSibling !== null) {
                        d3.event.target.parentElement.nextSibling.firstChild.focus();
                    } else {
                        self.list.node().firstChild.firstChild.focus();
                    }
                    break;
                }
            });
        self.current = self.root.append("span")
            .style("display", "inline-block")
            .style("box-sizing", "border-box")
            .style("overflow", "hidden")
            .style("text-overflow", "ellipsis")
            .style("vertical-align", "top")
            .style("width", "100%")
            .style("margin-left", "0")
            .style("margin-right", "0")
            .style("user-select", "none")
            .on("mousedown", function() {
                if (self.isOpen) {
                    self.close();
                } else {
                    self.open();
                }
            });
        self.list = self.root.append("ul")
            .style("position", "absolute")
            .style("box-sizing", "border-box")
            .style("z-index", "1000")
            .style("top", "100%")
            .style("left", "0")
            .style("max-height", "0")
            .style("visibility", "hidden")
            .style("list-style", "none")
            .style("margin", "0")
            .style("padding", "0")
            .style("min-width", "100%")
            .style("overflow-x", "hidden")
            .style("overflow-y", "auto");

        self.areAllSelected = function() {
            return self.entries.every(e => e.selected);
        };

        self.updateCurrent = function() {
            if (self.areAllSelected()){
                self.current.html(self.allLabel);
            } else if (self.entries.filter(e => e.selected).length > 0) {
                self.current.html(self.entries.filter(e => e.selected).map(e => e.name).join(", "));
            } else {
                self.current.html(self.noneLabel);
            }
        };

        self.updateEntries = function() {
            for (let entry of self.entries) {
                if (!("selected" in entry)) {
                    entry.selected = false;
                }
            }
            var listData = self.entries;
            var allSelected = self.areAllSelected();
            if (self.allEnabled) {
                listData = [{ allShortcut: true, name: self.allLabel }].concat(self.entries);
            }
            self.list.selectAll("li").remove();
            var d = self.list.selectAll("li").data(listData);
            var li = d.enter().append("li")
                .on("mouseenter", function() {
                    d3.event.target.firstChild.focus();
                });
            li.filter(e => "allShortcut" in e)
                .attr("class", "d3-widget-multiselect-all-shortcut");
            var idFunc = function(e) {
                if ("allShortcut" in e) {
                    return "d3-multiselect-all-" + instanceID;
                } else {
                    return "d3-multiselect-" + instanceID + "-" + e.id;
                }
            };
            li.append("input")
                .attr("id", idFunc)
                .attr("type", "checkbox")
                .on("focus", function(e) {
                    if (self.focusTimeout !== null) {
                        clearTimeout(self.focusTimeout);
                        self.focusTimeout = null;
                    }
                    d3.select(d3.event.target.parentNode)
                        .attr("class", ((("allShortcut" in e) ? "d3-widget-multiselect-all-shortcut " : "")
                                        + "d3-widget-entry-selected"));
                })
                .on("focusout", function(e) {
                    d3.select(d3.event.target.parentNode)
                        .attr("class", ("allShortcut" in e) ? "d3-widget-multiselect-all-shortcut" : "");
                })
                .on("change", function(e) {
                    if ("allShortcut" in e) {
                        if (this.checked) {
                            for (let entry of self.entries) {
                                entry.selected = true;
                            }
                            self.list.selectAll("input").filter(e => !("allShortcut" in e)).property("checked", false);
                        } else {
                            for (let entry of self.entries) {
                                entry.selected = false;
                            }
                        }
                    } else {
                        if (self.areAllSelected()) {
                            self.list.selectAll("input").filter(e => ("allShortcut" in e)).property("checked", false);
                            for (let entry of self.entries) {
                                entry.selected = false;
                            }
                        }
                        e.selected = this.checked;
                        if (self.areAllSelected()) {
                            self.list.selectAll("input").property("checked", e => ("allShortcut" in e));
                        }
                    }
                    self.updateCurrent();
                    if (self.onChange !== null) {
                        self.onChange(self.getSelection());
                    }
                })
                .property("checked", function(e) {
                    if ("allShortcut" in e) {
                        return allSelected;
                    } else {
                        return !allSelected && e.selected;
                    }
                });
            li.append("label")
                .attr("for", idFunc)
                .style("display", "inline-block")
                .style("margin", "0")
                .style("padding", "0")
                .style("min-width", "100%")
                .style("user-select", "none")
                .html(e => e.name);
            self.updateCurrent();
        };

        self.open = function() {
            self.list
                .style("max-height", "30em")
                .style("visibility", "");
            self.isOpen = true;
        };

        self.close = function() {
            self.list
                .style("max-height", "0")
                .style("visibility", "hidden");
            self.isOpen = false;
        };

        self.updateEntries();
    }

    multiSelect.prototype.node = function() { var root = this.root; return function() { return root.node(); }; };

    multiSelect.prototype.getEntries = function() {
        return this.entries;
    }

    multiSelect.prototype.setEntries = function(entries) {
        this.entries = entries;
        this.updateEntries()
    };

    multiSelect.prototype.getSelection = function() {
        if (this.areAllSelected() && this.allCondense) {
            return "all";
        }
        return this.entries.filter(e => e.selected).map(e => e.id);
    };

    multiSelect.prototype.setNoneLabel = function(label) {
        if (label === undefined) {
            this.noneLabel = DEFAULT_NONE_LABEL;
        } else {
            this.noneLabel = label;
        }
        this.updateCurrent();
    }

    multiSelect.prototype.allShortcut = function(enabled, condense, label) {
        this.allEnabled = !!enabled;
        this.allCondense = !!enabled && !!condense;
        if (label === undefined) {
            this.allLabel = DEFAULT_ALL_LABEL;
        } else {
            this.allLabel = label;
        }
        this.updateEntries();
    };

    multiSelect.prototype.on = function(event, callback) {
        switch (event) {
        case "change": this.onChange = callback; break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    multiSelect.prototype.dispatch = function(event) {
        switch (event) {
        case "change": if (this.onChange !== null) this.onChange(this.getSelection()); break;
        default: console.error("Unknown callback '" + event + "'");
        }
    };

    return multiSelect;
});
